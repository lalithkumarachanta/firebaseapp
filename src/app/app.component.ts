import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'lantronix-auth';
  userData: any = {};

  constructor(private _authService: AuthService) {
   
  }

  ngOnInit() {
    this._authService.user_observable.subscribe(user => {
      this.userData = user;
      console.log(user);
    });
  }

  logout() {
    if (this._authService.isLoggedIn) {
      this._authService.logout();
    }
  }
}
