import { TestBed, async } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { FormsModule } from '@angular/forms';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';

describe('AuthService', () => {
  let service: AuthService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireAuthModule
      ],
      declarations: [AuthService],
      providers: [
        AuthService,
        AngularFireAuth
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
