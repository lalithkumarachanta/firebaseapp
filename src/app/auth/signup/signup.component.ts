import { Component, OnInit } from '@angular/core';
import { authObj } from '../signin/signin.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  public authObj: authObj = {
    "email": "",
    "password": ""
  };

  signUpForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });


  error: any = "";
  isLoading: boolean = false;
  submitted: boolean = false;
  constructor(private _authService: AuthService) { }

  ngOnInit(): void {
  }

  submit(userDetails: authObj) {
    this.submitted = true;
    if (this.signUpForm.valid) {
      this._authService.register(userDetails.email, userDetails.password).then(res => {
        console.log(res);
      }, err => {
        this.error = err.message;
      })
    }
  }

}
