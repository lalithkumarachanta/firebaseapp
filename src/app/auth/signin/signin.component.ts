import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

export class authObj {
  "email": string;
  "password": string;
}

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SigninComponent implements OnInit {

  public authObj: authObj = {
    "email": "",
    "password": ""
  };

  signInForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  error: any = "";
  isLoading: boolean = false;
  constructor(private _authService: AuthService) { }

  ngOnInit(): void {
  }

  submit(auth: authObj) {
    if (this.signInForm.valid) {
      this.isLoading = true;
      this._authService.login(auth.email, auth.password).then(item => {
        console.log(item);
        this.isLoading = false;
      }, err => {
        console.log(err);
        this.error = err.message;
        this.isLoading = false;
      });
    }
  }
}
