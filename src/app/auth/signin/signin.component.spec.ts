import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninComponent } from './signin.component';
import { AuthService } from '../auth.service';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';

describe('SigninComponent', () => {
  let component: SigninComponent;
  let fixture: ComponentFixture<SigninComponent>;
  let authService: AuthService;
  let firebaseService:AngularFireAuth;
  let fakeLoginRes: any;
  let fakeLoginReq: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        AngularFireAuthModule
      ],
      declarations: [SigninComponent],
      providers: [
        AuthService,
        HttpClient,
        AngularFireAuth
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninComponent);
    component = fixture.componentInstance;


    localStorage.clear();
    authService = TestBed.get(AuthService);
    firebaseService = TestBed.get(AngularFireAuth);

    fakeLoginReq = { email: 'test@gmail.com', password: "test@123" };
    fakeLoginRes = {
      "kind": "identitytoolkit#VerifyPasswordResponse",
      "localId": "k55yvbAweHXXeO2HXCxozPed99A2",
      "email": "manasa@gmail.com",
      "displayName": "",
      "idToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjdkNTU0ZjBjMTJjNjQ3MGZiMTg1MmY3OWRiZjY0ZjhjODQzYmIxZDciLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbGFudHJvbml4LTNiNGU2IiwiYXVkIjoibGFudHJvbml4LTNiNGU2IiwiYXV0aF90aW1lIjoxNTkzMzU3ODYwLCJ1c2VyX2lkIjoiazU1eXZiQXdlSFhYZU8ySFhDeG96UGVkOTlBMiIsInN1YiI6Ims1NXl2YkF3ZUhYWGVPMkhYQ3hvelBlZDk5QTIiLCJpYXQiOjE1OTMzNTc4NjAsImV4cCI6MTU5MzM2MTQ2MCwiZW1haWwiOiJtYW5hc2FAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbIm1hbmFzYUBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.KVyOR0p2jgf0UJWlJ95avXySRl_eclEfgeHhx4wsjPTG81ygy7VAs7957UiYw5lw3mGvPiCuvPBl-exr2W20Khr47WDBXdpDxaeUEKzODh4fKYoENU3jXaOesoJ175H0nJjyA5oeJfb1VZalF1PjPj7dtZ4PojeG-tnuXmzgKMc7cXBQacJOEZabML8qX4dnPTIvyW-HBFCbGlKViNDOZhDqMfA2vqD1NqLpyeAipoRMYhexfhJ1YDZJj3AYiP-241Teu-58CiVanbBQsEQmzy3KkHe7nf0rJSKkUzI5ee7tCAbVuacw-6SSOLfzc-GcMdyDj-pUB_ZLiEz2Z1ywZg",
      "registered": true,
      "refreshToken": "AE0u-Nc70svxw942Zuf_Q5lE30ZRkOLIhl7Rgo63tSQ_OtjgpiHpKnP11OcV788ogyQ9jdbHJT0j2ELtmydQtSkef64p4iVW2ODwVhQPUcwtd5ozfnx1pK0VJonCOcXtGtQ2L99raqtaJlsZ6YedRHapIjmzOt6Yoj0gkA6w3P6616VkDJyTu0DiyZFfgsudsipmeBMgVWZ-tkBFN4A3B-hgzJtffjvaQQ",
      "expiresIn": "3600"
    }

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should declare submit', () => {
    expect(component.submit).toBeTruthy();
  });

  it('signin  toHaveBeenCalled', () => {
    spyOn(authService, 'login').and.returnValue(
      fakeLoginRes
    );
    spyOn(component, 'submit');
    component.submit(fakeLoginReq);
    expect(component.submit).toHaveBeenCalled();
    expect(component.isLoading).toBeTruthy();
  });

  it('should create login failed', () => {
    component.submit({ email: 'f', password: '' });
    fixture.detectChanges();
    expect(component.error).toBeTruthy();
    expect(component.isLoading).toBeTruthy();
  });

});
