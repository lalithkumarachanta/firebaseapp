import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'firebase';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  fireBaseUser: User;
  user_observable = new Subject();

  setDeleteIndex(input: User) {
    this.user_observable.next(input);
  }
  constructor(public _angFireAuth: AngularFireAuth, public _router: Router) {
    this._angFireAuth.authState.subscribe(user => {
      if (user) {
        this.fireBaseUser = user;
        localStorage.setItem('user', JSON.stringify(this.fireBaseUser));
        let userObj = JSON.parse(JSON.stringify(user));
        this.user_observable.next(userObj);
        this._router.navigate(['home/dashboard']);
      } else {
        localStorage.setItem('user', null);
        this.user_observable.next(null);
        this._router.navigate(['auth/signin']);
      }
    })
  }

  login(email: string, password: string) {
    return this._angFireAuth.signInWithEmailAndPassword(email, password);
  }

  async register(email: string, password: string) {
    return this._angFireAuth.createUserWithEmailAndPassword(email, password);
    // this.sendEmailVerification();
  }

  async logout() {
    await this._angFireAuth.signOut();
    localStorage.removeItem('user');
    this._router.navigate(['auth/signin']);
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
}
