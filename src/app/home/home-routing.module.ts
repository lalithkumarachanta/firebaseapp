import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WelcomeComponent } from './dashboard/welcome/welcome.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'dashboard',
  pathMatch: 'full'
},
{
  path: 'dashboard',
  component: DashboardComponent,
  children:[
      {
        path:'',
        redirectTo:'welcome',
        pathMatch:'full'
      },
      {
        path: 'welcome',
        component: WelcomeComponent,
      },
      { path: 'list', loadChildren: () => import('./dashboard/users/users.module').then(m => m.UsersModule) },
  ]
},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
