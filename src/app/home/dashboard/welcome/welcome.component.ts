import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  userDetails:any;
  constructor(private _authService:AuthService) { }

  ngOnInit(): void {
    this.userDetails = JSON.parse(localStorage.getItem('user')); 
    this.userDetails.createdAt = new Date(parseInt(this.userDetails.createdAt)); 
  }

}
