import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss']
})
export class UserslistComponent implements OnInit {

  usersList:any;
  constructor(private _userService:UserService) { }

  ngOnInit(): void {
    this.getUsersList();
  }

  getUsersList(){
    this._userService.getUsersList().subscribe(users=>{
      this.usersList = users;
    })
  }

}
