import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserslistComponent } from './userslist/userslist.component';

import { MatListModule, MatSelectionList } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { UsersRoutingModule } from './users-routing.module';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [UserslistComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatSelectModule,
    UsersRoutingModule,
    MatIconModule
  ]
})
export class UsersModule { }
