import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FireConfig } from './fire-config';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

var fireConfig = {
  apiKey: "AIzaSyB6EIFEN3zNP1JdmOe5ksCBHlybPJFQDvE",
  authDomain: "lantronix-3b4e6.firebaseapp.com",
  databaseURL: "https://lantronix-3b4e6.firebaseio.com",
  projectId: "lantronix-3b4e6",
  storageBucket: "lantronix-3b4e6.appspot.com",
  messagingSenderId: "289692336846"
}


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(fireConfig),
    AngularFireAuthModule,
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatGridListModule,
    HttpClientModule,
    MatListModule,
    MatIconModule,
  ],
  exports:[
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatGridListModule,
    RouterModule,
    MatListModule,
    MatIconModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
